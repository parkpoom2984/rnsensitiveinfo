using ReactNative.Bridge;
using System;
using System.Collections.Generic;
using Windows.ApplicationModel.Core;
using Windows.UI.Core;

namespace Sensitive.Info.RNSensitiveInfo
{
    /// <summary>
    /// A module that allows JS to share data.
    /// </summary>
    class RNSensitiveInfoModule : NativeModuleBase
    {
        /// <summary>
        /// Instantiates the <see cref="RNSensitiveInfoModule"/>.
        /// </summary>
        internal RNSensitiveInfoModule()
        {

        }

        /// <summary>
        /// The name of the native module.
        /// </summary>
        public override string Name
        {
            get
            {
                return "RNSensitiveInfo";
            }
        }
    }
}
