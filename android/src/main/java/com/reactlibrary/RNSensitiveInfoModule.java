package com.reactlibrary;
import android.annotation.TargetApi;
import android.os.Build;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.reactlibrary.Utils.Preference;

import java.security.KeyStore;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;

public class RNSensitiveInfoModule extends ReactContextBaseJavaModule {

  private static final String MODULE_NAME = "RNSensitiveInfo";

  private static final String ALGORITHM = "AES/GCM/NoPadding";
  private static final String PROVIDER = "AndroidKeyStore";
  private static final String KEYSTORE_ALIAS = "test";

  public RNSensitiveInfoModule(ReactApplicationContext reactContext) {
    super(reactContext);
  }

  @Override
  public String getName() {
    return MODULE_NAME;
  }

  @TargetApi(Build.VERSION_CODES.M)
  @ReactMethod
  public void rsaDecrypt(Promise promise) {
    String data = null;
    Preference preference = new Preference(getReactApplicationContext());
    try {
      KeyStore keyStore = KeyStore.getInstance(PROVIDER);
      keyStore.load(null);
      final KeyStore.SecretKeyEntry secretKeyEntry = (KeyStore.SecretKeyEntry) keyStore
              .getEntry(KEYSTORE_ALIAS, null);

      final SecretKey secretKey = secretKeyEntry.getSecretKey();
      final Cipher cipher = Cipher.getInstance(ALGORITHM);

      final GCMParameterSpec spec = new GCMParameterSpec(128, preference.getKeyIv());
      cipher.init(Cipher.DECRYPT_MODE, secretKey, spec);


      final byte[] decodedData = cipher.doFinal(preference.getEncryptedData());

      data = new String(decodedData);

      promise.resolve(data);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @TargetApi(Build.VERSION_CODES.M)
  @ReactMethod
  public void rsaEncrypt(String sensitiveInfo,Promise promise) {
    try {
      final KeyGenerator keyGenerator = KeyGenerator
              .getInstance(KeyProperties.KEY_ALGORITHM_AES, PROVIDER);
      final KeyGenParameterSpec keyGenParameterSpec = new KeyGenParameterSpec.Builder(KEYSTORE_ALIAS,
              KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT)
              .setBlockModes(KeyProperties.BLOCK_MODE_GCM)
              .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_NONE)
              .build();

      keyGenerator.init(keyGenParameterSpec);
      final SecretKey secretKey = keyGenerator.generateKey();

      final Cipher cipher = Cipher.getInstance(ALGORITHM);
      cipher.init(Cipher.ENCRYPT_MODE, secretKey);
      byte[] iv = cipher.getIV();
      byte[] encryptedData = cipher.doFinal(sensitiveInfo.getBytes());

      Preference preference = new Preference(getReactApplicationContext());
      preference.set(iv,encryptedData);
      promise.resolve(true);
    }catch (Exception e){
      e.printStackTrace();
    }
  }

}
