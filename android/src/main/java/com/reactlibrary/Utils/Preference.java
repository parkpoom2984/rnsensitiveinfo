package com.reactlibrary.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Base64;

public class Preference {
    private final SharedPreferences sharedPreferences;
    private static final String KEY_IV = "KEY_IV";
    private static final String ENCRYPTED_DATA = "ENCRYPTED_DATA";

    public Preference(Context context) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public final byte[] getKeyIv() {
        return Base64.decode(sharedPreferences.getString(KEY_IV, ""), Base64.DEFAULT);
    }

    public final byte[] getEncryptedData() {
        return Base64.decode(sharedPreferences.getString(ENCRYPTED_DATA, ""), Base64.DEFAULT);
    }

    public final void set(byte[] iv, byte[] encryptedData) {
        String ivBase64 = Base64.encodeToString(iv, Base64.DEFAULT);
        String encryptedDataBase64 = Base64.encodeToString(encryptedData, Base64.DEFAULT);
        sharedPreferences.edit().putString(KEY_IV, ivBase64).apply();
        sharedPreferences.edit().putString(ENCRYPTED_DATA, encryptedDataBase64).apply();
    }

    public final void delete() {
        sharedPreferences.edit().remove(KEY_IV).apply();
        sharedPreferences.edit().remove(KEY_IV).apply();
    }
}
