/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  Alert
} from 'react-native';


import { Container, Header, Content, Form, Item, Input, Label, Button, Text } from 'native-base'

import RNSensitiveInfo from 'react-native-sensitive-info'

export default class App extends Component {

  state = {
    text: ""
  }

  onPressSaveSensitiveData = () => {
    const sensitiveData = this.state.text
    if(sensitiveData != null && sensitiveData != '') {
       RNSensitiveInfo.rsaEncrypt(sensitiveData).then(success => {
        if(success){
          Alert.alert('save sensitive data already !!!')
          }
        })
    }else{
      Alert.alert('sensitive data is empty !!!')
    }
  }

  onPressGetSensitiveData = () => {
    RNSensitiveInfo.rsaDecrypt().then(data => {
      if(data != null && data != '') {
      Alert.alert(data)
      }else{
        Alert.alert('sensitive data not have on keystore !!!')
      }
    })
  }

  render() {
    return (
      <Container>
        <Content>
          <Form style={styles.form}>
            <Item floatingLabel last>
              <Label>PASSWORD</Label>
              <Input onChangeText={(text) => this.setState({text : text})}/>
            </Item>
            <Button block success style={styles.button} onPress ={() => this.onPressSaveSensitiveData()}>
              <Text>SAVE SENSITIVE DATA</Text>
            </Button>

            <Button block danger style={styles.button} onPress ={() => this.onPressGetSensitiveData()}>
              <Text>GET SENSITIVE DATA</Text>
            </Button>
          </Form>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    marginTop: 30
  },
  form: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 16
  }
});
